package com.kenfogel.fishfxtable.controller;

import com.kenfogel.fishfxtable.model.FishData;
import com.kenfogel.fishfxtable.persistence.FishDAO;
import java.sql.SQLException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

public class FishFXTableController {

    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    private FishDAO fishDAO;
    @FXML
    private AnchorPane fishFXTable;
    @FXML
    private TableView<FishData> fishDataTable;
    @FXML
    private TableColumn<FishData, Number> idColumn;
    @FXML
    private TableColumn<FishData, String> commonNameColumn;
    @FXML
    private TableColumn<FishData, String> latinColumn;
    @FXML
    private TableColumn<FishData, String> phColumn;
    @FXML
    private TableColumn<FishData, String> khColumn;
    @FXML
    private TableColumn<FishData, String> tempColumn;
    @FXML
    private TableColumn<FishData, String> fishSizeColumn;
    @FXML
    private TableColumn<FishData, String> speciesOriginColumn;
    @FXML
    private TableColumn<FishData, String> tankSizeColumn;
    @FXML
    private TableColumn<FishData, String> stockingColumn;
    @FXML
    private TableColumn<FishData, String> dietColumn;

    @FXML
    private ResourceBundle resources;

    /**
     * The constructor. The constructor is called before the initialize()
     * method.
     */
    public FishFXTableController() {
        super();
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        connectTableColumnsToFishData();
        calculateAndSetColumWidths();
        // Listen for selection changes and show the fishData details when
        // changed.
        fishDataTable.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> showFishDetails(newValue));

    }

    /**
     * Sets a reference to the FishDAO object that retrieves data from the
     * database
     *
     * @param fishDAO
     * @throws SQLException
     */
    public void setFishDAO(FishDAO fishDAO) throws SQLException {
        this.fishDAO = fishDAO;
    }

    public void loadTheTable() throws SQLException {
        // Add observable list data to the table
        fishDataTable.setItems(this.fishDAO.findAll());
    }

    /**
     * All access to the TableView to synchronize with Tree
     *
     * @return
     */
    public TableView<FishData> getTableView() {
        return fishDataTable;
    }

    /**
     * Private method that sets the column factory to the property type of the
     * FishData
     */
    private void connectTableColumnsToFishData() {
        idColumn.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());
        commonNameColumn.setCellValueFactory(cellData -> cellData.getValue().getCommonNameProperty());
        latinColumn.setCellValueFactory(cellData -> cellData.getValue().getLatinProperty());
        phColumn.setCellValueFactory(cellData -> cellData.getValue().getPhProperty());
        khColumn.setCellValueFactory(cellData -> cellData.getValue().getKhProperty());
        tempColumn.setCellValueFactory(cellData -> cellData.getValue().getTempProperty());
        fishSizeColumn.setCellValueFactory(cellData -> cellData.getValue().getFishSizeProperty());
        speciesOriginColumn.setCellValueFactory(cellData -> cellData.getValue().getSpeciesOriginProperty());
        tankSizeColumn.setCellValueFactory(cellData -> cellData.getValue().getTankSizeProperty());
        stockingColumn.setCellValueFactory(cellData -> cellData.getValue().getStockingProperty());
        dietColumn.setCellValueFactory(cellData -> cellData.getValue().getDietProperty());
    }

    /**
     * Private method that sets the widths of each column by assigning a
     * percentage of the total width
     */
    private void calculateAndSetColumWidths() {
        // fishDataTable.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        double width = fishFXTable.getPrefWidth();
        idColumn.setPrefWidth(width * .05);
        commonNameColumn.setPrefWidth(width * .15);
        latinColumn.setPrefWidth(width * .15);
        phColumn.setPrefWidth(width * .05);
        khColumn.setPrefWidth(width * .05);
        tempColumn.setPrefWidth(width * .05);
        fishSizeColumn.setPrefWidth(width * .1);
        speciesOriginColumn.setPrefWidth(width * .1);
        tankSizeColumn.setPrefWidth(width * .1);
        stockingColumn.setPrefWidth(width * .1);
        dietColumn.setPrefWidth(width * .1);

        log.info("Stage width is " + fishFXTable.getPrefWidth());
    }

    /**
     * To be able to test the selection handler for the table, this method
     * displays the Fish object that corresponds to the selected row.
     *
     * @param fishData
     */
    private void showFishDetails(FishData fishData) {
        System.out.println(fishData);
    }

}
