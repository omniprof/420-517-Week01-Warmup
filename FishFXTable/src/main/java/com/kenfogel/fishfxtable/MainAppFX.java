package com.kenfogel.fishfxtable;

import java.io.IOException;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import com.kenfogel.fishfxtable.persistence.FishDAO;
import com.kenfogel.fishfxtable.persistence.FishDAOImpl;
import com.kenfogel.fishfxtable.controller.FishFXTableController;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.scene.Parent;

/**
 * This is a demo of combining JDBC and JavaFX
 *
 * @author Ken
 */
public class MainAppFX extends Application {

    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    private Stage primaryStage;
    private Parent parent;
    private final FishDAO fishDAO;
    private Locale currentLocale;

    public MainAppFX() {
        super();
        fishDAO = new FishDAOImpl();
    }

    /**
     * The application starts here by configuring the Stage
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        initLocale();

        // Set the application icon using getResourceAsStream.
        this.primaryStage.getIcons().add(new Image(MainAppFX.class.getResourceAsStream("/images/bluefish_icon.png")));
        this.primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("Title"));

        initParent();

        primaryStage.show();
    }

    /**
     * The fxml and, if present, the css file are loaded and control passes to
     * the controller.
     */
    public void initParent() {

        try {
            // Instantiate the FXMLLoader
            FXMLLoader loader = new FXMLLoader();
            // Inform the loader of the location of the fxml file
            loader.setLocation(MainAppFX.class.getResource("/fxml/FishFXTable.fxml"));
            // Localize the loader with its bundle
            loader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));
            parent = (AnchorPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(parent);
            primaryStage.setScene(scene);

            // Give the controller access to the FishDAO
            FishFXTableController controller = loader.getController();
            controller.setFishDAO(fishDAO);

            // Display the database in a table
            controller.loadTheTable();

        } catch (IOException | SQLException e) {
            log.error("Error display table", e);
        }
    }

    /**
     * Manually change the locale
     */
    private void initLocale() {
        // Locale as returned by Java runtime
        Locale locale = Locale.getDefault();
        log.info("Locale = " + locale);

        // Manually changing the locale
        // currentLocale = new Locale("en","CA");
        currentLocale = new Locale("fr", "CA");

        // Alternative representation og locales
        // Locale currentLocale = Locale.CANADA;
        // Locale currentLocale = Locale.CANADA_FRENCH;
    }

    /**
     * Where is always begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
        System.exit(0);
    }
}
